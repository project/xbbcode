<?php

namespace Drupal\xbbcode\Plugin\XBBCode;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\xbbcode\Parser\Tree\TagElementInterface;
use Drupal\xbbcode\Plugin\TagPluginBase;
use Drupal\xbbcode\TagProcessResult;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a fallback placeholder plugin.
 *
 * BBCode tags will be assigned to this plugin when they are still enabled.
 *
 * @XBBCodeTag(
 *   id = "null",
 *   label = @Translation("[This tag is unavailable.]"),
 *   description = @Translation("The plugin providing this tag could not be loaded."),
 *   sample = @Translation("[{{ name }}]...[/{{ name }}]"),
 *   name = "null"
 * )
 */
class NullTagPlugin extends TagPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    LoggerChannelFactoryInterface $logger_factory,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->loggerFactory = $logger_factory;

    $this->loggerFactory->get('xbbcode')->alert('Missing BBCode tag plugin: %tag',
      [
        '%tag' => $plugin_id,
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function doProcess(TagElementInterface $tag): TagProcessResult {
    return new TagProcessResult((string) $tag->getOuterSource());
  }

}
